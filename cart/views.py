from django.shortcuts import render, get_object_or_404
from Amazon.models import Product
from django.contrib.auth.decorators import login_required
from .cart import Cart
from django.http import JsonResponse
from django.contrib import messages
from django.shortcuts import render,redirect, get_object_or_404

# Create your views here.
def cart_summary(request):
    cart = Cart(request)
    cart_products = cart.get_prods
    quantities=cart.get_quants
    totals = cart.cart_total()
    return render(request, 'cart_summary.html', {'cart_products': cart_products, 'quantities': quantities, 'totals':totals})

def cart_add(request):
    cart = Cart(request)
    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('product_id'))
        product_qty = int(request.POST.get('product_qty'))
        product = get_object_or_404(Product, id=product_id)
        cart.add(product=product, quantity=product_qty)
        messages.success(request, "Product Added to cart!")
        cart_quantity = cart.__len__()
        response = JsonResponse({'qty: ': cart_quantity})
        return response

def cart_delete(request):
    cart = Cart(request)
    if request.POST.get("action") == "post":
        product_id = int(request.POST.get('product_id'))
        cart.delete(product=product_id)
        response = JsonResponse({'product: ': product_id})
        return response

@login_required
def checkout(request):
    cart = Cart(request)
    totals = cart.cart_total()
    return render(request, 'checkout.html', { 'totals':totals})


def billing(request):
  if request.POST:
    my_shipping = request.POST
    request.session['my_shipping'] = my_shipping

  
  return render(request, 'billing.html')

def order_process(request):
  if request.POST:
    billing_form = billing(request.POST or None)
    my_shipping = request.session.get('my_shipping')
    
    payment_info = f"{my_shipping['shipping_address1']}\n{my_shipping['shipping_address2']}\n{my_shipping['shipping_state']}\n{my_shipping['shipping_city']}\n{my_shipping['shipping_zipcode']}\n{my_shipping['shipping_country']}"
    print(payment_info)
    messages.success(request, 'access denied')
    return redirect('home')

   

  else:
    messages.success(request, "access denied")
    return redirect('home')
  
    
    
    
    