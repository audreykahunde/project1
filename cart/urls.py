from django.urls import path
from . import views

urlpatterns = [
    path('cart_summary/', views.cart_summary, name="cart_summary"),
    path('add/', views.cart_add, name="cart_add"),
    path('delete/', views.cart_delete, name="cart_delete"),
    path('checkout/', views.checkout, name='checkout'),
    path('billing', views.billing, name='billing'),
    path('order_process', views.order_process, name='order_process')
]