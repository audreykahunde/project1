from Amazon.models import Product

class Cart():
    def __init__(self,request):
        self.session = request.session

        #Get the current session key if it exists
        cart = self.session.get('session_key')

        #If the user is new, create session key
        if 'session_key' not in request.session:
            cart = self.session['session_key'] = {}

        #Make sure cart is available on all pages of the site
        self.cart= cart

    def add(self,product, quantity):
        product_id = str(product.id)
        product_qty = str(quantity)

        if product_id in self.cart:
            pass
        else:
            self.cart[product_id ] = int(product_qty)

        self.session.modified = True
    
    def cart_total(self):
    # Get product ids
        product_ids = self.cart.keys()
        
        # Lookup the keys in the product model
        products = Product.objects.filter(id__in=product_ids)
        
        total = 0
        
        for product in products:
            quantity = self.cart[str(product.id)]  # Get the quantity of the current product
            sub_total= product.price * int(quantity) # Calculate the total price for this product
            total += sub_total  
        
        return total


    def __len__(self):
        return len(self.cart)
    def get_prods(self):
        #get ids from products
        product_ids = self.cart.keys()
        #use these to get actual products
        products = Product.objects.filter(id__in=product_ids)
        return products
    def get_quants(self):
        quantities = self.cart
        return quantities
    def delete(self, product):
        product_id =str(product)
        #delete from dict/cart
        if product_id in self.cart:
            del self.cart[product_id]
        self.session.modified = True




        