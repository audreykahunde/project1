from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path("login/", views.login_user, name='login'),
    path("logout/", views.logout_user, name="logout"),
    path('register/', views.register_user, name='register'),
    path('update_info/', views.update_info, name='update_info'),
    path('update_password/', views.update_password, name='update_password'),
    path('settings/', views.settings, name='settings'),
    path("report/", views.report, name='report'),
    path("generate_pdf/", views.generate_pdf, name='generate_pdf'),
    path('business_profile/<int:product_id>/', views.business_profile, name='business_profile'),
    path('add_product/', views.add_product, name='add_product'),
    path('delete/<int:product_id>/', views.delete_product, name='delete_product'),
    path('my_products/', views.my_products, name='my_products'),
    path('category/<str:foo>', views.category, name='category'),
    path('about/',views.about,name='about'),
    path('product/<int:pk>', views.product, name='product'),
    path('view/<int:product_id>/',views.view ,name='view'),
    path('search/',views.search ,name='search'),    
    
    
]