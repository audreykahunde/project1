from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django import forms

# Create your models here.

class Customer(models.Model):
	user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
	name = models.CharField(max_length=200, null=True)
	email = models.CharField(max_length=200)

	def __str__(self):
		return self.name
	
class Category(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name
    
    #@deverodd2011
    
    class Meta:
        verbose_name_plural = 'categories'
    
	
class Product(models.Model):
	seller = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
	name = models.CharField(max_length=200)
	price = models.DecimalField(max_digits=15, decimal_places=0)
	category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)
	image = models.ImageField(null=True, blank=True,default="no_image.png")
	description = models.TextField(null=True, blank=True)
	rating =models.IntegerField(default=0)
	total_ratings=models.IntegerField(default=0)
    

	
	def __str__(self):
		return self.name

	@property
	def imageURL(self):
		try:
			url = self.image.url
		except:
			url = ''
		return url
#file size validator
def file_size(value):
    limit = 10 * 1024 * 1024  # 10 MB in bytes
    if isinstance(value, (list, tuple)):
        for file in value:
            if file.size > limit:
                raise forms.ValidationError('File size should not exceed 10MB')
    else:
        if value.size > limit:
            raise forms.ValidationError('File size should not exceed 10MB')
		

def average_rating(self):
        reviews = Review.objects.filter(product=self)
        if reviews.exists():
            return sum([review.rate for review in reviews]) / reviews.count()
        return 0

@property
def total_ratings(self):
        return Review.objects.filter(product=self).count()
#create customer profile
class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	date_modified = models.DateTimeField(User, auto_now=True)
	contact = models.CharField(max_length=20, blank=True)
	address = models.CharField(max_length=200, blank=True)
	city = models.CharField(max_length=200, blank=True)
	street = models.CharField(max_length=200, blank=True)
	file1 = models.FileField(validators=[file_size], null=True, upload_to='uploads/')
	file2 = models.FileField(validators=[file_size], null=True, upload_to='uploads/')
	file3 = models.FileField(validators=[file_size], null=True, upload_to='uploads/')

	profile_pic = models.ImageField(null=True, blank=True, default="profile.png")
	def __str__(self):
		return self.user.username
#create a user profile by default when he/she signs up
def create_profile(sender, instance, created, **kwargs):
	if created:
		user_profile = Profile(user=instance)
		user_profile.save()
#automate the profile creation
post_save.connect(create_profile, sender=User)

class Order(models.Model):
	customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)
	date_ordered = models.DateTimeField(auto_now_add=True)
	complete = models.BooleanField(default=False)
	transaction_id = models.CharField(max_length=100, null=True)

	def __str__(self):
		return str(self.id)
	
	@property
	def get_cart_total(self):
		orderitems = self.orderitem_set.all()
		total = sum([item.get_total for item in orderitems])
		return total 

	@property
	def get_cart_items(self):
		orderitems = self.orderitem_set.all()
		total = sum([item.quantity for item in orderitems])
		return total 

class ShippingAddress(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, blank=True, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, blank=True, null=True)
    address = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=200, null=True)
    state = models.CharField(max_length=200, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.address
	

class Review (models.Model):
	user = models.ForeignKey(User,models.CASCADE)
	product = models.ForeignKey(Product, models.CASCADE)
	comment= models.TextField(max_length=250)
	rate = models.IntegerField(default=0)
	

	def __str__(self):
		return self(self.id)

