from django.contrib import admin
from django.contrib.auth.models import User 
from .models import Profile

from .models import *
# Register your models here.
admin.site.register(Customer)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Profile)
admin.site.register(Order)

#mix profile info and user info
class ProfileInline(admin.StackedInline):
    model = Profile

# extend user model
class UserAdmin(admin.ModelAdmin):
    model = User
    field = ["username", "email"]
    inlines = [ProfileInline]

#unregister the old way
admin.site.unregister(User)
#re-register the new way
admin.site.register(User, UserAdmin)

